const myNumber = require('..');

describe('Variable myNumber', () => {
  it('should be an object', async () => {
    expect(typeof myNumber).toBe('object');
  });

  it('should be an integer number', async () => {
    expect(Number.isInteger(Number(myNumber))).toBeTruthy();
  });

  it('shouldn\'t be a NaN value', async () => {
    expect(Number.isNaN(myNumber)).toBeFalsy();
  });

  it('should be an instance of Number constructor', async () => {
    expect(myNumber.constructor.toString().toLowerCase().includes('number')).toBeTruthy();
  });
});
